package chuiyuan.lsj.androidjava.datastructrue;

import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamField;
import java.io.Serializable;
import java.util.AbstractCollection;
import java.util.AbstractSet;
import java.util.Arrays;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Set;

/**
 * Created by lsj on 2015/9/5.
 * <p>All elements are permitted as keys or values, including null.</p>
 *
 * <p>Note that the iteration order for HashMap is non-deterministic. If you want
 * deterministic iteration , use {@link java.util.LinkedHashMap}.也就是说顺序不是固定的</p>
 *
 * <p>Note :the implementation of {@code MyHashMap} is not synchronized.
 * If one thread of several threads accessing an instance modifies the map
 * structurally, access to the map needs to be synchronized. A structural modification is an
 * operation that adds or removes an entry. Changes in the value of an entry are not
 * structural changes.</p>
 *
 * <p>The {@code Iterator} created by calling the {@code iterator} method may throw a
 * {@code ConcurrentModificationException} if the map is structurally changed while an iterator
 * is used to iterate over the elements. Only the {@code remove} method that is provided by
 * the iterator allows for removal of elements during iteration. It is not possible to
 * guarantee that this mechanism works in all caes of unsynchronized concurrent modification.
 * It should only be used for debugging purposes</p>
 */
public class MyHashMap <K,V> extends MyAbstractMap<K,V> implements Cloneable, Serializable{
    /**
     * must be a power of two (power 1-30)
     */

    private static final int MINIMUM_CAPACITY= 4;
    /**
     * max capacity for a HashMap
     */
    private static final int MAXIMUM_CAPACITY=1<<30;

    private static final MyMap.Entry[] EMPTY_TABLE =
            new HashMapEntry[MINIMUM_CAPACITY>>>1] ;

    static final float DEFAULT_LOAD_FACTOR = .75F ;
    /**
     * The hash table. If this hash map contains a mapping for null, it is
     * not represented this hash table.(不太明白)
     */
    transient  HashMapEntry<K,V> [] table;
    /**
     * the entry representing the null key, or null if there is no such mapping
     */
    transient  HashMapEntry<K,V>  entryForNullKey ;

    transient  int size ;
    /**
     * Incremented by "structural modifications" to allow (best effort)
     * detection of concurrent modification
     */
    transient  int modCount ;
    /**
     * The table is rehashed when its size exceeds this threshold.
     * The value of this filed is generally .75*capacity,except when the
     * capacity is zero, as described in the EMPTY_TABLE declaration
     * above.
     */
    private transient int threshold ;

    //Views - lazily initiazed
    private transient  Set<K> keySet ;
    private transient Set<MyMap.Entry<K,V>> entrySet ;
    private transient Collection<V> values ;

    /**
     * constructs a new empty {@code MyHashMap} instance
     */
    public MyHashMap(){
        table = (HashMapEntry<K,V> []) EMPTY_TABLE ;
        threshold = -1;
    }

    /**
     * Constructs a new {@code MyHashMap} instance with the specified capacity.
     * @throws IllegalArgumentException
     *              when the capacity is less than zero
     */
    public MyHashMap(int capacity ){
        if (capacity<0)
            throw new IllegalArgumentException("Capacity:"+ capacity);
        if (capacity ==0){
            HashMapEntry<K,V> [] tab = (HashMapEntry<K,V> []) EMPTY_TABLE ;
            table = tab ;
            threshold = -1;// forces fist put() to replace EMPTY_TABLE
            return;
        }
        if (capacity< MINIMUM_CAPACITY){
            capacity = MINIMUM_CAPACITY;
        }else if (capacity> MAXIMUM_CAPACITY){
            capacity = MAXIMUM_CAPACITY;
        }else {
            //capacity = Collections.roundUpToPowerOfTwo(capacity);
        }
        //makeTable(capacity) ;

    }

    /**
     * @throws IllegalArgumentException
     * when the capacity is less than 0 or the load factor is
     * less or equal to 0 or NaN
     */
    public MyHashMap(int capacity , float loadFactor){
        this(capacity) ;
        if (loadFactor<=0 || Float.isNaN(loadFactor)){
            throw new IllegalArgumentException("Load Factor:"+loadFactor) ;
        }
    }

    public MyHashMap(MyMap<? extends K, ?extends V> map){
        this(capacityForInitSize(map.size())) ;
    }

    /**
     * inserts all of the elements of map into this HashMap in a manner
     * suitable for use by constructors and pseudo-constructors(i.e.,clone,
     * readObjects).Also used by LinkedHashMap
     * @param map
     */
    final void constructorPutAll (MyMap<? extends K,? extends V> map){
        if (table == EMPTY_TABLE){

        }
        for (MyMap.Entry<? extends K, ? extends V> e : map.entrySet()){

        }
    }

    static int capacityForInitSize(int size){
        int result = (size>>1)+ size;//Multiply by 3/w to allow for growth

        //boolean expr is equivalent to result >-0 && result<MAXIMUM_CAPACITY
        return (result & ~(MAXIMUM_CAPACITY-1)) ==0 ? result: MAXIMUM_CAPACITY;
    }

    /**
     * a shallo copy of this map
     * @return
     */
    @Override
    public Object clone(){
        /**
         * could be more efficient
         */
        MyHashMap<K,V> result ;
        try {
            result = (MyHashMap<K,V>)super.clone() ;
        }catch (CloneNotSupportedException e){
            throw new AssertionError(e) ;
        }
        //restore clone to empty state, retaining our capacity and threshold
        //result.maketable (table.length) ;
        result.entryForNullKey = null ;
        result.size =0;
        result.keySet= null;
        result.values = null ;
        //give subclass a chance to initialized itself
        //result.init() ;
        result.constructorPutAll(this);
        return  result;
    }

    /**
     *
     */
    void init (){}


    @Override
    public boolean isEmpty (){
        return size ==0;
    }
    /**
     * @return the number of elements in this map
     */
    @Override
    public int size (){
        return  size;
    }

    public V get(Object key ){
        if (key== null){
            HashMapEntry<K,V> e = entryForNullKey;
            return  e == null? null : e.value;
        }
        int hash = 0;// Collections.secondaryHash(key);
        HashMapEntry<K,V> [] tab = table;
        for (HashMapEntry<K,V> e = tab[hash&(tab.length-1)];
                e!= null ; e=e.next){
            K eKey = e.key;
            if (eKey== key|| (e.hash== hash && key.equals(eKey))){
                return e.value;
            }
        }
        return  null;
    }

    /**
     * return if this map contains the specific key
     * @param key can be null
     * @return
     */
    @Override
    public boolean containsKey (Object key){
        if (key== null){
            return  entryForNullKey!= null ;
        }
        int hash =0; //= Collections.secondaryHash(key) ;
        MyHashMap.HashMapEntry<K,V> [] tab = table;
        //HashMapEntry放到了tab[hash&(tab.length-1)
        for (HashMapEntry<K,V> e = tab[hash&(tab.length-1)];
                e!= null; e= e.next){
            K eKey = e.key;
            if (eKey== key || (e.hash== hash&&key.equals(eKey))){
                return  true;
            }
        }
        return  false;
    }

    @Override
    public boolean containsValue (Object value){
        HashMapEntry [] tab = table ;
        int len = tab.length ;
        if (value== null){
            for (int i =0;i<len ; i++){
                for (HashMapEntry e = tab[i] ;e!= null; e= e.next){
                    if (e.value == null){
                        return  true;
                    }
                }
            }
            return entryForNullKey!= null && entryForNullKey.value== null;
        }
        //value is non-null
        for (int i =0;i<len; i++){
            for (HashMapEntry e = tab[i]; e!= null; e= e.next) {
                if (value.equals(e.value)) {
                    return true;
                }
            }
        }
        return  entryForNullKey!= null && value.equals(entryForNullKey.value);
    }

    /**
     *
     * @param key
     * @param value
     * @return  the value of previous mapping with specific key
     * or null if there was no such mapping
     */
    @Override
    public V put (K key ,V value){
        if (key == null){
            return putValueForNullKey(value);
        }

        int hash =0;  //Collections.secondaryHash(key) ;
        HashMapEntry <K,V> [] tab = table ;
        int index = hash&(tab.length-1) ;
        for (HashMapEntry<K,V> e = tab[index] ;e!= null;e = e.next){
            if (e.hash== hash&& key.equals(e.key)){
                preModify(e);
                V oldValue = e.value;
                e.value = value;
                return  oldValue;
            }
        }
        // No entry for (non-null) key is present,create one
        modCount ++;
        if (size++ > threshold){
            tab = doubleCapacity();
            index = hash& (tab.length-1);
        }
        addNewEntry(key, value ,hash, index);
        return  null;
    }

    private V putValueForNullKey( V value){
        HashMapEntry<K, V> entry = entryForNullKey ;
        if (entry == null){
            //如果之前没有entryNullKey
            addNewEntryForNullKey(value);
            size++ ;
            modCount++;
            return  null;
        }else {
            //如果之前有
            preModify(entry); //给LinkedHashMap一个修改的机会
            V oldValue = entry.value;
            entry.value = value;
            return oldValue;
        }
    }

    /**
     * Give LinkedHashMap a chance to take action when we modifying
     * an exiting entry
     * @param e the entry to be modified
     */
    void preModify(HashMapEntry<K,V> e ){}

    /**
     *Create a new entry and inserts it into hash table.
     * This is overridden by LinkedHashMap
     */
    void addNewEntry( K key, V value , int hash ,int index){
        table[index] = new HashMapEntry<K,V>(key, value,hash, table[index]) ;
    }

    /**
     *Create a new entry for the null key with the given value.
     * This value is overridden by LinkedHashMap
     */
    void addNewEntryForNullKey(V value){
        entryForNullKey = new HashMapEntry<K,V>(null, value,0,null);
    }

    HashMapEntry<K,V> constructorNewEntry(K key,V value, int hash,
                                          HashMapEntry<K,V> first){
        return  new HashMapEntry<>(key, value, hash, first);
    }

    /**
     * This method is just like put, except that it does not do somethings
     * ie....
     */
    private void constructorPut (K key ,V value){
        if (key== null){
            HashMapEntry<K,V> entry = entryForNullKey ;
            if (entry == null){
                entryForNullKey = constructorNewEntry(null, value,0, null);
                size++;
            }else {
                entry.value = value;
            }
            return;
        }

        int hash =0; // Collections.secondaryHash(key) ;
        HashMapEntry<K,V> [] tab = table ;
        int index= hash&(tab.length-1);
        HashMapEntry<K,V> first = tab[index] ;
        for (HashMapEntry<K,V> e = first; e!= null; e= e.next){
            if (e.hash== hash&& key.equals(e.key)){
                e.value = value;
                return;
            }
        }
        // no entry for (non-null) key is present
        tab[index] = constructorNewEntry(key, value , hash, first) ;
        size++;
    }

    @Override
    public void putAll(MyMap<? extends K, ? extends V>map ){
        ensureCapacity(map.size()) ;
        super.putAll(map);
    }

    private void ensureCapacity(int munMappings){
        int newCapacity = 0; // Collections.roundUpToPowerOfTwo(capacityForInitSize(munMappings));
        HashMapEntry<K,V> [] oldTable = table ;
        int oldCapacity = oldTable.length;
        if (newCapacity<= oldCapacity){
            return;
        }
        if (newCapacity == oldCapacity*2){
            doubleCapacity();
            return;
        }
        //growing up by at leats 4x,rehash in the obvious way
        HashMapEntry<K,V>[] newTable = makeTable(newCapacity );
        if (size!=0){
            int newMask = newCapacity-1 ;
            for (int i =0;i<oldCapacity ;i++){
                for (HashMapEntry<K,V> e= oldTable[i]; e!=null;e= e.next){
                    HashMapEntry<K,V> oldNext = e.next;
                    int newIndex = e.hash& newMask;
                    HashMapEntry<K,V> newNext = newTable[newIndex] ;
                    newTable[newIndex] =e ;
                    e.next = newNext;
                    e = oldNext;
                }
            }
        }
    }

    /**
     *Allocate a table of the given capacity and set the threshold
     */
    private HashMapEntry<K,V> []  makeTable (int newCapacity){
        HashMapEntry<K,V> [] newTable =
                (HashMapEntry<K,V> []) new HashMapEntry[newCapacity];
        table = newTable ;
        threshold = (newCapacity>>1)+ (newCapacity>>2);//0.75
        return  newTable;

    }

    /**
     *Doubles the capacity of the hash table. Existing entries are
     * placed in the correct bucket on the enlarged table.
     */
    private HashMapEntry<K,V> [] doubleCapacity(){
        HashMapEntry<K,V> [] oldTable = table;
        int oldCapacity = oldTable.length;
        if (oldCapacity == MAXIMUM_CAPACITY){
            return oldTable;
        }
        int newCapacity = oldCapacity*2 ;
        HashMapEntry<K,V>[] newTable = makeTable(newCapacity) ;
        if (size ==0){
            return  newTable;
        }

        for (int j =0; j<oldCapacity;j++){
            /*
             *Rehash the bucket using the minimum number of filed writes.
             * This is the most subtle and delicate code in the class
             */
            HashMapEntry<K,V> e = oldTable[j] ;
            if (e== null){
                continue;
            }
            int highBit = e.hash& oldCapacity;
            HashMapEntry<K,V> broken = null;
            newTable[j|highBit] = e;
            for (HashMapEntry<K,V> n = e.next ;n!= null ;e = n,n = n.next){
                int nextHighBit = n.hash& oldCapacity;
                if (nextHighBit!= highBit){
                    if (broken== null){
                        newTable[j|nextHighBit] =n ;
                    }else {
                        broken.next = n;
                    }
                    broken = e;
                    highBit = nextHighBit ;
                }
            }
            if (broken != null){
                broken.next = null;
            }
        }
        return  newTable ;
    }

    /**
     *Removes the mapping with specific key
     */
    @Override
    public V remove (Object key){
        if (key == null){
            return  removeNullKey();
        }
        int hash = 0;// Collections.secondaryHash(key);
        HashMapEntry<K,V>[] tab = table ;
        int index = hash& (tab.length-1); //找到所在的List
        for (HashMapEntry<K,V> e= tab[index], pre = null;
                e != null ; pre = e, e= e.next){
            if (e.hash == hash&& key.equals(e.key)){
                if (pre == null){
                    tab[index] = e.next;
                }else {
                    pre.next = e.next;
                }
                modCount++;
                size --;
                postRemove(e);
                return  e.value ;
            }
        }
        return null;
    }

    private V removeNullKey(){
        HashMapEntry<K,V> e = entryForNullKey;
        if (e==null){
            return  null ;
        }
        entryForNullKey = null;
        modCount++;
        size --;
        postRemove(e) ;
        return  e.value;
    }

    /**
     *Subclass overrides this method to unlink entry
     * 不知道做什么的
     */
    void postRemove(HashMapEntry<K,V> e){}

    @Override
    public void clear (){
        if (size!= 0){
            Arrays.fill(table, null);
            entryForNullKey = null ;
            modCount++;
            size =0;
        }
    }

    @Override
    public Set<K> keySet (){
        Set<K> ks = keySet ;
        return (ks!= null) ? ks:(keySet = new KeySet());
    }

    @Override
    public Collection<V> values (){
        Collection<V> vs = values;
        return (vs!=null)? vs:(values= new Values()) ;
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        Set<MyMap.Entry<K,V>> es = entrySet;
        return (es!= null)? es :(entrySet = new EntrySet());
    }

    static class HashMapEntry<K,V> implements MyMap.Entry<K,V>{
        final  K key ;
        V value ;
        final int hash ;
        HashMapEntry<K,V> next ;
        HashMapEntry(K key, V value, int hash, HashMapEntry<K,V> next){
            this.key = key;
            this.value = value;
            this.hash = hash ;
            this.next = next;
        }
        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public V setValue(V value) {
            V oldValue = this.value ;
            this.value = value;
            return  oldValue;
        }
    }

    private abstract class HashItertor {
        int nextIndex ;
        HashMapEntry<K,V> nextEntry = entryForNullKey;
        HashMapEntry<K,V> lastEntryReturned ;
        int expectedModCount = modCount;

        HashItertor(){
            if (nextEntry == null){
                HashMapEntry<K,V> next = null ;
                HashMapEntry<K,V> [] tab = table ;
                while(next == null&& nextIndex<tab.length){
                    next = tab[nextIndex] ;
                }
                nextEntry = next ;
            }
        }

        public boolean hasNext (){
            return  nextEntry!= null ;
        }
        HashMapEntry<K,V> nextEntry(){
            if (modCount!= expectedModCount)
                throw new ConcurrentModificationException();
            if (nextEntry == null)
                throw new NoSuchElementException();
            HashMapEntry<K,V> entryToReturn = nextEntry ;
            HashMapEntry<K,V> [] tab = table ;
            HashMapEntry<K,V> next = entryToReturn.next ;
            while(next== null&& nextIndex< tab.length){
                next = tab[nextIndex++] ;
            }
            nextEntry = next ;
            return  lastEntryReturned = entryToReturn;
        }
        public void remove (){
            if (lastEntryReturned == null)
                throw new IllegalArgumentException( );
            if (modCount!= expectedModCount)
                throw new ConcurrentModificationException() ;
            MyHashMap.this.remove(lastEntryReturned) ;
            lastEntryReturned = null ;
            expectedModCount = modCount ;
        }

    }

    private final class KeyIterator extends HashItertor
            implements Iterator<K> {
        public K next (){
            return nextEntry().key ;
        }
    }

    private final class ValueIterator extends HashItertor
            implements Iterator<V >{
        public V next (){
            return  nextEntry().value ;
        }
    }

    private final class EntryIterator extends HashItertor
            implements Iterator<MyMap.Entry<K,V>>{
        public Entry<K,V> next(){
            return  nextEntry() ;
        }
    }

    private boolean containsMapping (Object key ,Object value){
        if (key == null){
            HashMapEntry<K,V> e = entryForNullKey ;
            return  e != null&& Objects.equals(value,e.value) ;
        }
        int hash = 0;// Collections.secondaryHash(key);
        HashMapEntry<K,V> [] tab = table ;
        int index = hash& (tab.length-1) ;
        for (HashMapEntry<K,V> e = tab[index]; e!= null;
                e = e.next){
            if (e.hash == hash&& key.equals(e.key)){
                return Objects.equals(value, e.value) ;
            }
        }
        return false ; //no entry for the key
    }

    private boolean removeMapping(Object key , Object value){
        if (key == null){
            HashMapEntry<K,V> e = entryForNullKey ;
            if (e == null|| !Objects.equals(value, e.value)){
                return  false ;
            }
            entryForNullKey = null ;
            modCount ++;
            size --;
            postRemove(e);
            return  true ;
        }
        int hash = 0;//Collections.secondaryHash(key) ;
        HashMapEntry<K,V> [] tab = table ;
        int index = hash&(tab.length-1) ;
        for (HashMapEntry<K,V> e = tab[index] ,pre = null;
                e!= null; pre = e, e = e.next){
            if (e.hash == hash && key.equals(e.key)){
                if (!Objects.equals(value, e.value)){
                    return  false ;
                }
                if (pre == null){
                    tab [index ] = e.next ;
                }else {
                    pre.next = e.next ;
                }
                modCount ++;
                size --;
                postRemove(e);
                return  true ;
            }
        }
        return  false ;//no entry for key
    }

    //Subclass(LinkedHashMap) overrides these for correct iteration and order
    Iterator<K> newKeyIterator (){
        return new KeyIterator();
    }
    Iterator<V> newValueIterator(){
        return new ValueIterator();
    }
    Iterator<MyMap.Entry<K,V>> newEntryIterator(){
        return  new EntryIterator();
    }

    private final class KeySet extends AbstractSet<K>{

        @Override
        public Iterator<K> iterator() {
            return newKeyIterator();
        }

        @Override
        public int size() {
            return size;
        }

        @Override
        public boolean contains(Object object) {
            return containsKey(object);
        }

        @Override
        public boolean remove(Object object) {
            int oldSize = size;
            MyHashMap.this.remove(object) ;
            return size!= oldSize ;
        }

        @Override
        public void clear() {
            MyHashMap.this.clear();
        }
    }

    private final class Values extends AbstractCollection<V>{
        @Override
        public Iterator<V> iterator() {
            return newValueIterator();
        }

        @Override
        public int size() {
            return size;
        }

        @Override
        public boolean isEmpty() {
            return size==0 ;
        }

        @Override
        public void clear() {
            MyHashMap.this.clear();
        }

        @Override
        public boolean contains(Object object) {
            return containsValue(object) ;
        }
    }

    private final class EntrySet extends AbstractSet<MyMap.Entry<K,V>>{
        @Override
        public Iterator<Entry<K, V>> iterator() {
            return newEntryIterator() ;
        }
        @Override
        public boolean contains(Object o){
            if (!(o instanceof Entry))
                return false ;
            Entry<?,?> e = (Entry<?,?>) o;
            return containsMapping(e.getKey(), e.getValue()) ;
        }
        @Override
        public boolean remove (Object object){
            if (!(object instanceof Entry)){
                return false;
            }
            Entry<?,?> e = (Entry<?,?>)object ;
            return removeMapping(e.getKey(), e.getValue()) ;
        }
        @Override
        public int size() {
            return size;
        }
        @Override
        public boolean isEmpty (){
            return size==0;
        }

        @Override
        public void clear (){
            MyHashMap.this.clear();
        }
    }
    // what is this
    private static final ObjectStreamField[] serialPersistentFields={
            new ObjectStreamField("loadFactor", float.class)
    };

    private void writeObject(ObjectOutputStream stream) throws IOException{
        //emulate loadFactor field for other implementations to read
        ObjectOutputStream.PutField fields = stream.putFields();
        fields.put("loadFactor", DEFAULT_LOAD_FACTOR);
        stream.writeFields();
        stream.writeInt(table.length);//Capacity
        stream.writeInt(size);

        for (Entry<K,V> e :entrySet()){
            stream.writeObject(e.getKey());
            stream.writeObject(e.getValue());
        }
    }

    private void readObject(ObjectInputStream stream) throws IOException ,
    ClassNotFoundException{
        stream.defaultReadObject();
        int capacity = stream.readInt();
        if (capacity<0){
            throw new InvalidObjectException("capacity:"+ capacity);
        }
        if (capacity<MINIMUM_CAPACITY){
            capacity= MINIMUM_CAPACITY;
        }else if (capacity> MAXIMUM_CAPACITY){
            capacity = MAXIMUM_CAPACITY;
        }else {
            capacity = capacity;// Collections.roundUpToPowerOfTwo(capacity) ;
        }

        makeTable(capacity) ;
        int size = stream.readInt();
        if (size<0){
            throw new InvalidObjectException("Size:"+ size);
        }
        init();//GIve subclass (LinkedHashMap) a chanceto initialize itself
        for (int i=0; i<size ; i++){
            K key = (K) stream.readObject();
            V values = (V) stream.readObject() ;
            constructorPut(key, values);
        }
    }



}
