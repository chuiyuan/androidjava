package chuiyuan.lsj.androidjava.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.widget.Toast;

/**
 * Service通过BroadCast广播与Activity通信
 * sendBroadcast from Service--->Android System--->
 * Receive by broadcastReceiver in Activity
 */
public class BroadcastService extends Service {
    private String TAG ="BroadcastService" ;

    public class MyBinder extends Binder{
        public BroadcastService getService(){
            return BroadcastService.this;
        }
    } ;

    public void sendServiceBroadcast() throws InterruptedException{
        Toast.makeText(BroadcastService.this, "download in thread:"+ Thread.currentThread().getName(),
                Toast.LENGTH_SHORT).show();
        Intent intent = new Intent() ;
        intent.setAction("chuiyuan.lsj.androidjava.service.broadcastservice") ;
        intent.putExtra("value", 1000) ;
        sendBroadcast(intent);
        Toast.makeText(BroadcastService.this, "send over", Toast.LENGTH_LONG).show();
    }

    public BroadcastService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return  new MyBinder();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //如果 是用的startService，可以在这里执行
//        Toast.makeText(this, "onCreate:send broadcast", Toast.LENGTH_SHORT).show();
//        try {
//            sendServiceBroadcast();
//        }catch (InterruptedException e){
//            e.printStackTrace();
//        }

    }

    @Override
    public void onDestroy() {
        Toast.makeText(this, "onDestroy", Toast.LENGTH_SHORT).show();
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "onStartCommand", Toast.LENGTH_SHORT).show();
        return super.onStartCommand(intent, flags, startId);
    }
}
