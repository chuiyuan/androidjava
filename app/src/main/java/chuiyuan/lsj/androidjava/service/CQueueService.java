package chuiyuan.lsj.androidjava.service;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

import chuiyuan.lsj.androidjava.algorithm.CQueue;
import chuiyuan.lsj.androidjava.view.CQueueActivity;

/**
 * this is remote service
 */
public class CQueueService extends Service {
    private  final String TAG ="CQueueService";

    public static final int MSG_NO = 0;

    class IncomingHandler extends  Handler{
        @Override
        public void handleMessage(Message msg) {
            if (msg.replyTo != null){
                //收到消息后就向客户端发送一个回应,实际上可以单独拿出来
                Message msg_to_client = this.obtainMessage() ;
                msg_to_client.what = CQueueActivity.MSG_NO_TO_CLIENT;
                Bundle bundle= new Bundle();
                try {
                    bundle.putString("CQueue", CQueue.test());
                    msg_to_client.setData(bundle);
                }catch (Exception e){

                }

                try {
                    //得到客户端的Messenger,并向它发送消息
                    cMessenger = msg.replyTo ;
                    cMessenger.send(msg_to_client);
                }catch (RemoteException e){

                }
            }
            switch (msg.what){
                case MSG_NO:
                    Toast.makeText(CQueueService.this, "msg from client", Toast.LENGTH_LONG).show();
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }


    /**
     * 自己的Messenger
     */
    Messenger sMessenger= new Messenger(new IncomingHandler());
    //客户端的Messenger
    Messenger cMessenger ;
    public CQueueService() {
    }

    @Override
    public void onCreate() {
        Log.e(TAG, "onCreate");
        super.onCreate();
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.e(TAG, "onBind");
        //返回自己Messenger的binder,以供客户端通过Binder(new Messenger(binder))得到Service的Messenger对象。
        return sMessenger.getBinder();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.e(TAG, "onUnbind");
        return super.onUnbind(intent);
    }
}
