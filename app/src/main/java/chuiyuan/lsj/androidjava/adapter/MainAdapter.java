package chuiyuan.lsj.androidjava.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import chuiyuan.lsj.androidjava.R;
import chuiyuan.lsj.androidjava.base.BaseContentAdapter;

/**
 * Created by lsj on 2015/8/30.
 */
public class MainAdapter extends BaseContentAdapter<String> {

    public MainAdapter(Context mContext, List<String> mList) {
        super(mContext, mList);
    }

    @Override
    public View getConvertView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null ;
        if (convertView == null){
            viewHolder = new ViewHolder() ;
            convertView = mInflater.inflate(R.layout.item_main, null) ;
            viewHolder.textView = (TextView)convertView.findViewById(R.id.item_main_tv) ;
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder)convertView.getTag() ;
        }
        viewHolder.textView.setText(getItem(position));
        return convertView;
    }

    private class ViewHolder {
        public TextView textView ;
    }


}
