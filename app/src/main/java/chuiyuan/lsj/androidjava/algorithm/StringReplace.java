package chuiyuan.lsj.androidjava.algorithm;

/**
 * Created by lsj on 2015/8/29.
 * sword offer 4:
 * replace " " with "%20
 */
public class StringReplace {
    /**
     *
     * @param input
     * @param sub1  to be replaced
     * @param sub2  replace sub1 with sub2
     * @return
     */
    public  String replaceBlank(String input){
        if (input == null){
            return  null;
        }
        StringBuilder sb = new StringBuilder() ;
        for (int i=0;i<input.length() ;i++){
            if (input.charAt(i)==' '){ //如果是空格
                sb.append("%") ;
                sb.append("2");
                sb.append("0");
            }else {
                sb.append(String.valueOf(input.charAt(i))) ;
            }
        }
        return  new String(sb) ;

    }

    public static String test(){
        String s = "here is a girl" ;
        return  new StringReplace().replaceBlank(s) ;
    }

}
