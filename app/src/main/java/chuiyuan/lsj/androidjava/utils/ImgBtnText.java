package chuiyuan.lsj.androidjava.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import chuiyuan.lsj.androidjava.R;

/**
 * Created by lsj on 2015/9/26.
 */
public class ImgBtnText extends LinearLayout{
    private View root ;
    private ImageButton imageButton ;
    private TextView textView ;

    public ImgBtnText(Context context){
        super(context, null);
    }

    public ImgBtnText(Context context, AttributeSet attrs){
        super(context, attrs);
        root =LayoutInflater.from(context).inflate(R.layout.imgbtn_text, this, true);
        imageButton = (ImageButton)root.findViewById(R.id.imgbtn1) ;
        textView = (TextView)root.findViewById(R.id.text1);

        TypedArray ar = context.obtainStyledAttributes(attrs,R.styleable.ImgBtnText);
        CharSequence text = ar.getText(R.styleable.ImgBtnText_android_text);
        if (text!=null) textView.setText(text);
        Drawable drawable = ar.getDrawable(R.styleable.ImgBtnText_android_src);
        if (drawable!= null) imageButton.setImageDrawable(drawable);

        ar.recycle();
    }

    public void setButtonImageResource(int resId){
        imageButton.setImageResource(resId);
    }

    public void setTextViewText(String text){
        textView.setText(text);
    }
}
