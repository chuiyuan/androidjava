package chuiyuan.lsj.androidjava.view;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import chuiyuan.lsj.androidjava.R;
import chuiyuan.lsj.androidjava.base.BaseActivity;
import chuiyuan.lsj.androidjava.service.CQueueService;

/**
 * this is to test remote service
 */
public class CQueueActivity extends BaseActivity{
    public static final int MSG_NO_TO_CLIENT=0;

    ServiceConnection mSc ;

    /**
     * handler of incoming msg from serivce
     */
    class IncomingHandler extends Handler{
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case MSG_NO_TO_CLIENT:
                    Toast.makeText(CQueueActivity.this,"ack_msg from service", Toast.LENGTH_LONG).show();
                    Bundle bundle = msg.getData();
                    Toast.makeText(CQueueActivity.this,bundle.getString("CQueue"), Toast.LENGTH_LONG).show();
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    } ;
    /**
     * 自己的Messenger
     */
    Messenger mMessenger = new Messenger(new IncomingHandler()) ;
    //remote Service 的Messenger
    Messenger sMessenger ;

    @Override
    protected void findView() {
        setContentView(R.layout.activity_cqueue);
    }

    @Override
    protected void initView() {
        mSc = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                Log.d(TAG, "onServiceConnected");
                //得到服务端的Messenger
                sMessenger = new Messenger(service);
                //向服务端发送消息
                sendMsg() ;

            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                Log.d(TAG, "onServiceDisconnected");

            }
        } ;
    }

    /**
     * 使用服务端的Messenger向它发送消息
     */
    private void sendMsg(){
        Message msg_to_service = new Message();
        msg_to_service.replyTo =  mMessenger;
        try{
            sMessenger.send(msg_to_service);
        }catch (RemoteException e ){
            e.printStackTrace();
        }
    }


    @Override
    protected void onStop() {
        super.onStop();
        this.unbindService(mSc);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, this.getApplicationContext().getPackageCodePath()) ;
        Intent service  = new Intent(this, CQueueService.class) ;
        this.bindService(service, mSc, Context.BIND_AUTO_CREATE) ;
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    protected void setOnClickListener() {

    }
}
