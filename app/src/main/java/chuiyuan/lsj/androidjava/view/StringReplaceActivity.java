package chuiyuan.lsj.androidjava.view;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import chuiyuan.lsj.androidjava.R;
import chuiyuan.lsj.androidjava.base.BaseActivity;
import chuiyuan.lsj.androidjava.service.StrReplaceService;

/**
 * Created by lsj on 2015/8/30.
 */
public class StringReplaceActivity extends BaseActivity {

    private TextView resultTv ;
    private ServiceConnection sc = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            strReplaceService = ((StrReplaceService.MyBinder)service).getService() ;
            //这个可以在onClick中调用
            doService();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };
    private StrReplaceService strReplaceService ;
    String result ="string" ;

    @Override
    public void onClick(View v) {

    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, StrReplaceService.class);
        //onCreate时候已经有了sc
        bindService(intent, sc , Context.BIND_AUTO_CREATE) ;
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindService(sc);
    }

    @Override
    protected void findView() {
        setContentView(R.layout.activity_str_replace);
    }

    @Override
    protected void initView() {
        resultTv = (TextView)findViewById(R.id.str_replace_resultTv);


    }

    /**
     * 调用StrReplaceService中的方法
     */
    private void doService(){
        result = strReplaceService.strReplace() ;
        Log.d(TAG, result) ;
        resultTv.setText(result);
    }

    @Override
    protected void setOnClickListener() {

    }
}
