package chuiyuan.lsj.androidjava.view;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.Toast;

import chuiyuan.lsj.androidjava.R;
import chuiyuan.lsj.androidjava.base.BaseActivity;
import chuiyuan.lsj.androidjava.service.BroadcastService;

public class ServiceSendbroadcastActivity extends BaseActivity{

    private BroadcastService broadcastService;

    private ServiceConnection sc = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            broadcastService = ((BroadcastService.MyBinder)service).getService();
            try{
                broadcastService.sendServiceBroadcast();
            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    } ;

    @Override
    protected void findView() {
        setContentView(R.layout.activity_service_sendbroadccast);
    }

    @Override
    protected void initView() {
        //广播注册
        IntentFilter filter = new IntentFilter() ;
        filter.addAction("chuiyuan.lsj.androidjava.service.broadcastservice");
        registerReceiver(serviceReceiver, filter) ;
    }

    @Override
    protected void setOnClickListener() {

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindService(sc);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent bindIntent = new Intent(this, BroadcastService.class) ;
        bindService(bindIntent, sc, BIND_AUTO_CREATE);
    }

    public BroadcastReceiver serviceReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle extras = intent.getExtras() ;
            if (extras!= null){
                if (extras.containsKey("value")){
                    //这里可以做下载，发包等
                    Toast.makeText(ServiceSendbroadcastActivity.this,
                            "receive broadcast:"+extras.get("value"), Toast.LENGTH_SHORT).show();

                }
            }
        }
    } ;


}
