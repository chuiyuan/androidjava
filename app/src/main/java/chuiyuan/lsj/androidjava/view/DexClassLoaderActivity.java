package chuiyuan.lsj.androidjava.view;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import chuiyuan.lsj.androidjava.R;
import chuiyuan.lsj.androidjava.base.BaseActivity;
import dalvik.system.DexClassLoader;

public class DexClassLoaderActivity extends BaseActivity {
    private TextView textView;

    @Override
    protected void findView() {
        setContentView(R.layout.activity_dex_class_loader);
    }

    @Override
    protected void initView() {
        textView = (TextView)findViewById(R.id.dex_loadTv);
        useDexClassLoader();
    }

    @Override
    protected void setOnClickListener() {

    }

    public void useDexClassLoader(){
        Intent intent = new Intent("com.example.lsj.testapp.plugin.client", null);
        PackageManager pm = getPackageManager();
        final List<ResolveInfo> plugins = pm.queryIntentActivities(intent, 0);
        ResolveInfo rinfo = plugins.get(0);
        ActivityInfo ainfo = rinfo.activityInfo;

        String div = System.getProperty("path.separator");
        String packageName = ainfo.packageName;
        String dexPath = ainfo.applicationInfo.sourceDir;
        String dexOutputDir = getApplicationInfo().dataDir;
        String libPath = ainfo.applicationInfo.nativeLibraryDir;

        DexClassLoader dexClassLoader = new DexClassLoader(dexPath,
                dexOutputDir,libPath,this.getClass().getClassLoader());
        try{
            Class <?> clazz = dexClassLoader.loadClass(packageName+".PluginClass");
            Object obj = clazz.newInstance() ;
            Class [] params = new Class[2];
            params[0]= Integer.TYPE;
            params[1] = Integer.TYPE ;
            Method action = clazz.getMethod("function1", params) ;
            Integer ret = (Integer)action.invoke(obj, 1,2);
            Log.e(TAG, "return value is :"+ ret);
            textView.setText(ret.toString());
        }catch ( ClassNotFoundException e){

        }catch (InstantiationException e2 ){

        }catch (IllegalAccessException e3){

        }catch (NoSuchMethodException e4){

        }catch (InvocationTargetException e5){

        }
    }

    @Override
    public void onClick(View v) {

    }
}
