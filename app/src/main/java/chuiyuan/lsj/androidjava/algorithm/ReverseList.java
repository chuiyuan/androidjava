package chuiyuan.lsj.androidjava.algorithm;

import java.util.Stack;

/**
 * Created by lsj on 2015/8/30.
 * sword offer 5:
 * 倒序打出List
 */
public class ReverseList {

    private static class ListNode{
        int data ;
        ListNode next ;

        public ListNode(int data, ListNode next) {
            this.data = data;
            this.next = next;
        }
    }

    public  String  printListReverse(ListNode headNode){
        Stack<ListNode> stack = new Stack<>() ;
        StringBuilder sb = new StringBuilder() ;
        if (headNode== null){
            sb.append("List empty") ;
        }
        while(headNode!= null){
            stack.push(headNode) ;
            headNode = headNode.next;
        }

        while (!stack.isEmpty()){
            sb.append(stack.pop().data) ;
        }
        return  new String(sb); //
    }

    public String test (){
        ListNode node1 = new ListNode(1,null) ;
        ListNode node2 = new ListNode(2, null) ;
        ListNode node3 = new ListNode(3, null) ;
        node1.next = node2 ;
        node2.next = node3 ;
        return printListReverse(node1) ;
    }
}
