package chuiyuan.lsj.androidjava.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import chuiyuan.lsj.androidjava.R;

/**
 * Created by lsj on 2015/9/26.e
 */
public class BallView2  extends View{
    private float x ;
    private float y ;
    private float r  ;
    private int color ;

    public BallView2(Context context){
        super(context, null);
    }
    public BallView2(Context context, AttributeSet attrs){
        super(context, attrs);
        //得到自定义的属性
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.BallView) ;
        //在xml没有定义这个属性时，使用默认值 30
        x = ta.getFloat(R.styleable.BallView_BallStartX,30) ;
        y = ta.getFloat(R.styleable.BallView_BallStartY,30);
        r = ta.getFloat(R.styleable.BallView_BallRadius,30);
        color = ta.getColor(R.styleable.BallView_BallColor, Color.GREEN);

        ta.recycle(); //一定要
    }

    @Override
    public void onDraw(Canvas canvas){
        Paint paint = new Paint() ;
        paint.setColor(color);
        canvas.drawCircle(x, y, r, paint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        x = event.getX();
        y = event.getY();
        invalidate(); //刷新
        return  true;  //成功
    }
}
