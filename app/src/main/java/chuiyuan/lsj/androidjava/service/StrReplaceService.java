package chuiyuan.lsj.androidjava.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import chuiyuan.lsj.androidjava.algorithm.StringReplace;

/**
 * Created by lsj on 2015/8/29.
 * 这是一个LocalService
 */
public class StrReplaceService extends Service {
    private final String TAG="StrReplaceService" ;

    public class MyBinder extends Binder{
        public StrReplaceService getService(){
            return StrReplaceService.this ;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind");
        return  new MyBinder() ;
    }


    @Override
    public boolean onUnbind(Intent intent) {
        Log.d(TAG, "onUnbind");
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        super.onDestroy();
    }

    public String strReplace (){
        String result = StringReplace.test() ;
        Log.d(TAG,result) ;
        return result ;
    }
}
