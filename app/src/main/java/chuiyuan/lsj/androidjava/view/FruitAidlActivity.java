package chuiyuan.lsj.androidjava.view;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import chuiyuan.lsj.androidjava.IFruitBinder;
import chuiyuan.lsj.androidjava.R;
import chuiyuan.lsj.androidjava.base.BaseActivity;
import chuiyuan.lsj.androidjava.service.FruitAidlService;

/**
 * Created by lsj on 2015/9/16.
 */
public class FruitAidlActivity extends BaseActivity {
    private Button btn ;
    private IFruitBinder iFruitBinder;

    private ServiceConnection sc = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            iFruitBinder = IFruitBinder.Stub.asInterface(service);

        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            iFruitBinder = null;
        }
    } ;

    @Override
    protected void findView() {
        setContentView(R.layout.activity_aidl);
    }

    @Override
    protected void initView() {
        btn = (Button)findViewById(R.id.aidl_button);
       // Intent serviceIntent = new Intent(this, FruitAidlService.class);
        //bindService(serviceIntent, sc,BIND_AUTO_CREATE);
    }

    @Override
    protected void setOnClickListener() {
        btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.aidl_button:
                if (iFruitBinder == null){
                    Log.d(TAG, "ibinder == null");
                }else {
                    try{
                        String str = "getFruit:"+ iFruitBinder.getFruit().getName()+
                                iFruitBinder.getFruit().getColor()+ iFruitBinder.getFruit().getNumber();
                        Log.d(TAG, str);
                    }catch (RemoteException e ){
                        e.printStackTrace();
                    }
                }
                break;
            default:
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent serviceIntent = new Intent(this, FruitAidlService.class);
        bindService(serviceIntent, sc, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindService(sc);
    }
}
