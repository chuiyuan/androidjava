package chuiyuan.lsj.androidjava.algorithm;

import java.util.Stack;

/**
 * Created by lsj on 2015/8/30.
 * sword offer 7
 * 用两个stack实现一个queue
 */
public class CQueue<E>{

    private Stack <E> stack1 = new Stack<>();
    private Stack<E> stack2 = new Stack<>() ;

    public void enqueue(E element){
        stack1.push(element) ;
    }

    /**
     * 如果stack2中为空
     * 1.stack1不为空，则要将stack1中的元素放入stack2中再dequeue
     * 2.stack1为空，则出错
     * 如果stack2不为空，直接弹出
     * @return
     */
    public E dequeue() throws Exception{
        if (stack2.isEmpty()){
            while(!stack1.isEmpty()){
                stack2.push(stack1.pop());
            }
        }
        if (stack2.isEmpty()){
            throw new Exception("queue is empty");
        }
        return  stack2.pop();
    }

    public static String test () throws  Exception{
        CQueue<String> queue = new CQueue<>() ;
        queue.enqueue("1");
        queue.enqueue("2");
        queue.enqueue("3");

        return queue.dequeue() ;
    }

}
