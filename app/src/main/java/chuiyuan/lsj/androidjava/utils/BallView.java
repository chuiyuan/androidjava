package chuiyuan.lsj.androidjava.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by lsj on 2015/9/26.
 */
public class BallView extends View{
    private float x = 40;
    private float y =50;
    private float r =15 ;

    public BallView(Context context, AttributeSet attr){
        super(context, attr);
    }

    @Override
    public void onDraw(Canvas canvas){
        super.onDraw(canvas);
        //创建画笔
        Paint paint= new Paint();
        //设置画笔颜色
        paint.setColor(Color.RED);
        //画circle
        canvas.drawCircle(x,y,r,paint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        setX(event.getX());
        setY(event.getY());
        invalidate(); //刷新
        return true; //一定要返回true,说明成功了
    }

    @Override
    public float getX() {
        return x;
    }

    @Override
    public void setX(float x) {
        this.x = x;
    }

    @Override
    public float getY() {
        return y;
    }

    @Override
    public void setY(float y) {
        this.y = y;
    }

    public float getR() {
        return r;
    }

    public void setR(float r) {
        this.r = r;
    }
}
