package chuiyuan.lsj.androidjava.view;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;

import chuiyuan.lsj.androidjava.IMyAIDL;
import chuiyuan.lsj.androidjava.R;
import chuiyuan.lsj.androidjava.base.BaseActivity;
import chuiyuan.lsj.androidjava.service.AIDLService;

/**
 * Created by lsj on 2015/9/12.
 */
public class AIDLActivity  extends BaseActivity{
    private IMyAIDL iMyAIDL;
    //private Intent binderIntent ;
    private final static boolean createFlag = true;
    private final static boolean destroyFlag = false;

    private ServiceConnection sc = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            iMyAIDL = IMyAIDL.Stub.asInterface(service) ;
            try{
                //通过AIDL远程调用，客户端，只进行调用
                Log.d(TAG,"start download") ;
                iMyAIDL.downLoad();
            }catch (RemoteException e ){
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    } ;
    @Override
    public void onClick(View v) {

    }

    @Override
    protected void findView() {
        setContentView(R.layout.activity_aidl);
    }

    @Override
    protected void initView() {
        //开启服务
        Intent intent = new Intent(this, AIDLService.class) ;
        startService(intent);

        //连接远程Service和activity
        Intent binderIntent = new Intent(this, AIDLService.class) ;
        Bundle bundle = new Bundle() ;
        bundle.putBoolean("flag", createFlag);
        binderIntent.putExtras(bundle) ;
        bindService(binderIntent, sc, BIND_AUTO_CREATE) ;
    }

    @Override
    protected void setOnClickListener() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy") ;
        boolean flag = false;
        //暂停服务
        Intent intent = new Intent(this, AIDLService.class) ;
        stopService(intent) ;
        //断开与远程Service的连接,更好的是放在stop里面，参考FruitAidlActivity
        unbindService(sc);
    }
}
