/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class chuiyuan_lsj_androidjava_jni_HelloNDK */

#ifndef _Included_chuiyuan_lsj_androidjava_jni_HelloNDK
#define _Included_chuiyuan_lsj_androidjava_jni_HelloNDK
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     chuiyuan_lsj_androidjava_jni_HelloNDK
 * Method:    syaHello
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_chuiyuan_lsj_androidjava_jni_HelloNDK_syaHello
  (JNIEnv *, jobject);

#ifdef __cplusplus
}
#endif
#endif
