package chuiyuan.lsj.androidjava.datastructrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by lsj on 2015/9/24.
 * 使用LinkedHashMap实现，有两个好处：
 * 1、LinkedHashMap已经实现了按照访问顺序的存储。也就是说最近读取的会放在前面，
 * 最不常读取的会放在最后。（当然它也可以按照插入顺序存储）
 * 2、LinkedHashMap本身有一个方法可以判断是否要移除最不常读取的数据，默认是不移除。
 * 所以我们要override 这个方法，使得当缓存里存放的数据个数超过规定个数后，就移除最不常用的。
 */
public class MyLRUCache1<K,V>{

    private static final float hashTableLoadFactor = 0.75f;

    private LinkedHashMap<K,V > map ;
    private int cacheSize ;

    public MyLRUCache1( int cacheSize){
        this.cacheSize = cacheSize ;
        int hashTableCapacity = (int)Math.ceil(cacheSize/hashTableLoadFactor)+1;
        //an anonymous inner class,true means accessOrder
        map = new LinkedHashMap<K,V>(hashTableCapacity, hashTableLoadFactor, true) {
            private static final long serialVersionID=1;
            @Override
            protected boolean removeEldestEntry(Entry<K, V> eldest) {
                //在什么情况下进行回收
                return size()> MyLRUCache1.this.cacheSize ;
            }
        };

    }

    public synchronized V get(K key){
        return map.get(key) ;
    }

    public synchronized void put(K key,V value){
        map.put(key, value) ;
    }

    public synchronized void clear (){
        map.clear();
    }

    public synchronized int usedEntries(){
        return map.size() ;
    }

    public synchronized Collection<Map.Entry<K,V>> getAll(){
        return new ArrayList<Map.Entry<K,V>>(map.entrySet()) ;
    }
}







