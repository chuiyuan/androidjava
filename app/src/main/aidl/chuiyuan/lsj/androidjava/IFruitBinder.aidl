// IAidlBinder.aidl
package chuiyuan.lsj.androidjava;
import chuiyuan.lsj.androidjava.Fruit;
// Declare any non-default types here with import statements

interface IFruitBinder {
    String getInfo ();
    Fruit getFruit();
}



