package chuiyuan.lsj.androidjava.base;

import android.app.Activity;
import android.os.Bundle;
import android.view.View ;

/**
 * Created by lsj on 2015/8/29.
 */
public abstract  class BaseActivity extends Activity implements View.OnClickListener {
    protected final String TAG = this.getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);

        findView();

        initView();

        setOnClickListener();
    }

    /**
     * 得到布局文件
     */
    protected abstract void findView() ;

    /**
     * 初始化View的一些数据
     */
    protected abstract void initView();

    /**
     * 设置监听
     */
    protected abstract void setOnClickListener();

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


}
