package chuiyuan.lsj.androidjava.datastructrue;


import java.util.Collection;
import java.util.Set;

/**
 * Created by lsj on 2015/9/5.
 */
public interface MyMap <K,V>{

    public static interface Entry<K,V>{
        /**
         * to be equal,they must have the same key and value
         * @param object
         * @return
         */
        boolean equals(Object object) ;

        K getKey ();

        V getValue();

        /**
         * Objects which are equal returns the same hashCode
         * @return
         */
        int hashCode ();

        V setValue (V object);

    };

    public void clear() ;

    public boolean containsKey (Object key ) ;

    public boolean containsValue (Object value ) ;

    Set<MyMap.Entry<K,V>> entrySet ();

    boolean equals (Object object) ;

    V get(Object key) ;

    int hashCode ();

    boolean isEmpty() ;

    Set <K> keySet ();

    V put (K key ,V value) ;

    void putAll (MyMap<? extends K, ? extends V> map) ;

    V remove (Object key );

    int size () ;

    public Collection<V> values() ;

}
