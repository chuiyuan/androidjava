package chuiyuan.lsj.androidjava.datastructrue;

import java.io.Serializable;
import java.util.AbstractCollection;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by lsj on 2015/9/5.
 * A base class for {@code Map} implementations.
 *
 * <p>Subclasses are that permit new mapping to be added must override
 * {@link #put}</p>
 *
 * <p>The default implementations of many methods are inefficient for many large maps,
 * for example in the default implementaion,each call to {@link #get} performs a linear
 * iteration of the entry set. Subclasses should override this to improve
 * their performance </p>
 */
public abstract class MyAbstractMap <K,V> implements  MyMap<K,V>{
    //lazily-initialied key set
    Set<K> keySet ;

    //lazily-initialized values
    Collection<V> valuesConllection ;

    /**
     *An imutable key-value mapping
     */
    public static class SimpleImutableEntry<K, V>
    implements MyMap.Entry<K,V>, Serializable{
        private static final long serialVersionUID = 7138329143939025153L;

        private final K key ;
        private final V value;

        public SimpleImutableEntry(K key , V value){
            this.key = key ;
            this.value = value ;
        }

        public SimpleImutableEntry(MyMap.Entry<? extends K, ? extends V > copyFrom){
            this.key = copyFrom.getKey() ;
            this.value = copyFrom.getValue() ;
        }
        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public V setValue(V object) {
            throw new UnsupportedOperationException() ;
        }

        @Override
        public boolean equals(Object object) {
           if (this == object){
               return  true ;
           }
            if (object instanceof MyMap.Entry){
                MyMap.Entry<?,?> entry = (MyMap.Entry<?,?>)object;
                return (key==null? entry.getKey()== null: key.equals(entry.getKey()))
                        && (value == null? entry.getValue()== null: value.equals(entry.getValue())) ;
            }
            return  false ;
        }

        /**
         * 使用的key.hash^value.hash
         * @return
         */
        @Override
        public int hashCode() {
            return  (key== null? 0: key.hashCode())
                    ^(value == null? 0: value.hashCode()) ;
        }

        @Override
        public String toString() {
            return key+"="+value ;
        }
    }

    /**
     * A key-value mapping with mutable values
     */
    public static class SimpleEntry <K,V> implements MyMap.Entry<K,V>, Serializable{
        private static final long serialVersionUID = -8499721149061103584L;

        private final K key ;
        private V value ; //imutable

        public SimpleEntry(K key, V value) {
            this.key = key ;
            this.value = value ;
        }

        public SimpleEntry(MyMap.Entry<? extends K, ? extends V> copyFrom){
            this.key = copyFrom.getKey();
            this.value = copyFrom.getValue() ;
        }

        @Override
        public boolean equals(Object object) {
            if (this == object)
                return  true;
            if (object instanceof MyMap.Entry){
                MyMap.Entry<?,? > entry = (MyMap.Entry<?,?>) object ;
                return (key == null? entry.getKey()== null: key.equals(entry.getKey()))
                        && (value == null? entry.getValue()== null: key.equals(entry.getValue())) ;
            }
            return  false ;
        }

        @Override
        public int hashCode() {
            return (key== null?0:key.hashCode())
                    ^ (value == null?0:value.hashCode()) ;
        }

        @Override
        public String toString() {
            return key+"="+ value;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public V setValue(V object) {
            V result = value ;
            value = object ;
            //返回的原来的值
            return result;
        }
    }

    protected MyAbstractMap(){

    }
    @Override
    public void clear() {
        entrySet().clear();
    }

    public abstract Set<MyMap.Entry<K,V>> entrySet () ;
    /**
     * iterates its key set ,looking for a key that
     * {@code key} equals
     * @param key can be null
     * @return
     */
    @Override
    public boolean containsKey(Object key) {
        Iterator<MyMap.Entry<K,V>> it = entrySet().iterator() ;
        if(key!= null){
            while (it.hasNext()){
                if (key.equals(it.next().getKey())){
                    return  true ;
                }
            }
        }else {
            while (it.hasNext()){
                if (it.next().getKey()== null){
                    return  true ;
                }
            }
        }
        return false;
    }

    /**
     * iterates its entry set ,looking for a entry with value
     * that {@code value} equals
     * @param value can be null
     * @return
     */
    @Override
    public boolean containsValue(Object value) {
        Iterator<MyMap.Entry<K,V>> it = entrySet().iterator();
        if (value!= null){
            while(it.hasNext()){
                if (value.equals(it.next().getValue())){
                    return  true;
                }
            }
        }else {
            while (it.hasNext()){
                if (it.next().getValue() == null){
                    return  true ;
                }
            }
        }
        return false;
    }

    /**
     * first checks the structure of {@code object}.
     * 1.If it is not a map or of a different size ,returns false.
     * 2.Otherwise ,iterates its own entry set ,looking up each entry's
     * key in {@code object}, If any values does not equals to the other map's
     * value for the same key, it returns false .
     * 3.Otherwise it returns true
     * @param object
     * @return
     */
    @Override
    public boolean equals(Object object) {
        if (this == object) //比较的是地址
            return  true ;
        if (object instanceof MyMap){
            MyMap<?,?> myMap = (MyMap<?,?>) object ;
            if (size()!= myMap.size()){
                return  false ;
            }
            try {
                for (MyMap.Entry<K,V> entry: entrySet()){
                    K key = entry.getKey() ;
                    V mine = entry.getValue();
                    //looking up each entry's key in myMap
                    Object theirs = myMap.get(key) ;
                    if (mine == null){
                        if (theirs!= null || !myMap.containsKey(key)){
                            return  false ;
                        }
                    }else if (!mine.equals(theirs)){
                        return  false ;
                    }
                }
            }catch (NullPointerException ignored){
                return  false;
            }catch (ClassCastException ignored){
                return  false ;
            }
            return true;
        }
        return  true ;

    }

    /**
     *iterates its entry set ,looking for an entry with a key
     * that {@code key} equals
     */
    @Override
    public V get(Object key) {
        Iterator<MyMap.Entry<K,V>> it = entrySet().iterator();
        if (key!= null){
            while (it.hasNext()){
                MyMap.Entry<K,V> entry = it.next() ;
                if (key.equals(entry.getKey())){
                    return  entry.getValue() ;
                }
            }
        }else {
            while (it.hasNext()){
                MyMap.Entry<K,V> entry = it.next() ;
                if (entry.getKey() == null){
                    return  entry.getValue() ;
                }
            }
        }
        return null;
    }

    /**
     * iterates its entry set ,summing the hashcode of its entry
     */
    @Override
    public int hashCode() {
        int result =0;
        Iterator<MyMap.Entry<K,V>> it = entrySet().iterator();
        while (it.hasNext()){
            result+= it.next().hashCode() ;
        }
        return  result ;
    }

    @Override
    public boolean isEmpty() {
        return size() ==0;
    }

    /**
     * 有点复杂啊，fuck
     * it returns a view that calls through this to map .
     * its iterator transforms this map's entry set iterator to return keys
     * 这里返回的是一个AbstractSet,且实现了其iterator的hasNext ,next, remove方法
     * @return
     */
    @Override
    public Set<K> keySet() {
        if (keySet == null){
            keySet = new AbstractSet<K>() {
                @Override
                public boolean contains(Object object) {
                    return containsKey(object) ;
                }

                @Override
                public Iterator<K> iterator() {
                    return new Iterator<K>() {
                        Iterator<MyMap.Entry<K,V>> setIterator = entrySet().iterator() ;
                        @Override
                        public boolean hasNext() {
                            return setIterator.hasNext() ;
                        }

                        @Override
                        public K next() {
                            return setIterator.next().getKey();
                        }

                        @Override
                        public void remove() {
                            setIterator.remove();
                        }
                    };
                }

                @Override
                public int size() {
                    return size();
                }
            } ;
        }
        return keySet;
    }

    @Override
    public V put(K key, V value) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void putAll(MyMap<? extends K, ? extends V> map) {
        for (MyMap.Entry<? extends K,? extends V>entry : map.entrySet() ){
            put(entry.getKey() , entry.getValue()) ;
        }
    }

    /**
     * iterates its entry set ,removing the entry with a key
     * that {@code key} equals
     * return the values to the removed key
     */
    @Override
    public V remove(Object key) {
        Iterator <MyMap.Entry<K,V>> it = entrySet().iterator() ;
        if (key!= null){
            while (it.hasNext()){
                MyMap.Entry<K,V> entry = it.next() ;
                if (key.equals(entry.getKey())){
                    it.remove();
                    return entry.getValue() ;
                }
            }
        }else {
            while (it.hasNext()){
                MyMap.Entry<K,V> entry = it.next() ;
                if (entry.getKey() == null){
                    it.remove();
                    return  entry.getValue();
                }
            }
        }
        return null;
    }

    @Override
    public int size() {
        return entrySet().size();
    }

    /**
     * returns a view that calls through this to map
     * the iterator transforms this map's entry set iterator to return values
     * @return
     */
    @Override
    public Collection<V> values() {
        if (valuesConllection == null){
            valuesConllection = new AbstractCollection<V>() {
                @Override
                public boolean contains(Object object) {
                    return containsValue(object) ;
                }

                @Override
                public Iterator<V> iterator() {
                    return  new Iterator<V>() {
                        Iterator<MyMap.Entry<K,V>> setIterator = entrySet().iterator() ;

                        @Override
                        public boolean hasNext() {
                            return setIterator.hasNext();
                        }

                        @Override
                        public V next() {
                            return setIterator.next().getValue();
                        }

                        @Override
                        public void remove() {
                            setIterator.remove();
                        }
                    };

                }

                @Override
                public int size() {
                    return size();
                }
            };
        }
        return valuesConllection;
    }
    @SuppressWarnings("unchecked")
    @Override
    protected Object clone() throws CloneNotSupportedException {
        MyAbstractMap<K,V> result = (MyAbstractMap<K,V>) super.clone();
        result.keySet = null ;
        result.valuesConllection = null;
        return result;
    }

    /**
     * This implementation composes a string by iterating its entry set, if this
     * map contains itself as a key or a value,this string "(this Map)"
     * will apear in its place
     * @return
     */
    @Override
    public String toString() {
        if (isEmpty()){
            return "{}";
        }
        // why 28
        StringBuilder buffer = new StringBuilder(size()*28) ;
        buffer.append('{') ;//
        Iterator<MyMap.Entry<K,V>> it = entrySet().iterator();
        while (it.hasNext()){
            MyMap.Entry<K,V> entry = it.next() ;
            Object key = entry.getKey() ;
            if (key!= this){
                buffer.append(key) ;
            }else {
                buffer.append("(this Map)");
            }
            buffer.append('=') ;//
            Object value = entry.getValue();
            if (value!= this){
                buffer.append(value);
            }else {
                buffer.append("(this Map)");
            }
            if (it.hasNext()){
                buffer.append(',');
            }
            buffer.append('}');
            return  buffer.toString();
        }
        return super.toString();
    }
}
