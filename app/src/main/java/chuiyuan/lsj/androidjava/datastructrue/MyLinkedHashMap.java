package chuiyuan.lsj.androidjava.datastructrue;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created by lsj on 2015/9/20.
 * LinkedHashMap is an implementation of {@link MyMap} that guarantees iteration order .
 * All optional operations are supported .
 *
 * <p> All elements are permitted as keys or values, including null.</p>
 *
 * <p>Entries are kept in a double-linked list. The iteration is ,by default, the
 * order in which keys are inserted. Reinserting an already present key does not change
 * the order. If the three argument constructor is used, and{@code accessOrder} is
 * {@code true}, the iteration will be in the order that the entries were accessed.
 * The access order is affected by {@code put},{@code get}, and {@code putAll} operations.
 * but not by operations on the collection views.</p>
 *
 * <p>Note: the implementations of {@code LinkedHashMap} is not synchronized, if one
 * thread of several threads accessing an instance modifies the map
 * structurally, access to the map needs to be synchronized. For
 * insertion-ordered instances a structural modification is an operation that
 * removes or adds an entry. Access-ordered instances also are structurally
 * modified by {@code put} ,{@code get}, and {@code putAll}, since these methods
 * change the order of the entries. Changes in the value of an entry are not
 * structurally changes.</p>
 *
 * <p>The {@code Iterator} created by calling the {@code iterator} method may
 * throw a {@code ConcurrentModificationException} if the map is structurally
 * changed while an iteration is used to iterate over the elements. Only
 * the {@code remove} method that is provided by the iteration allows for removal of
 * elements during iteration. It is not possible to guarantee that this mechanism
 * works in all cases of unsynchronized concurrent modification.</p>
 */
public class MyLinkedHashMap<K,V> extends  MyHashMap<K,V> {
    /**
     * A dummy entry in the circular linked list entries in the map.
     * The first real entry is header.nxt, and the last is header.prv.
     * If the map is empty, header.nxt == header.prv&& header.prv = header
     */
    transient LinkedEntry<K,V> header ;
    /**
     * True if access ordered, false if insertion ordered
     */
    private final boolean accessOrder;

    public MyLinkedHashMap(){
        init();
        accessOrder = false ;
    }
    //with a specific capacity
    public MyLinkedHashMap(int initialCapacity){
        this(initialCapacity, DEFAULT_LOAD_FACTOR) ;
    }

    public MyLinkedHashMap(int initialCapacity,float loadFactor){
        this(initialCapacity, loadFactor, false);
    }

    public MyLinkedHashMap( int initialCapacity, float loadFactor, boolean accessOrder){
        super(initialCapacity, loadFactor);
        init();
        this.accessOrder  = accessOrder ;
    }

    public MyLinkedHashMap(MyMap<? extends K, ? extends V> map){
        this(capacityForInitSize(map.size()));
        constructorPutAll(map);
    }

    @Override
    void init (){
        header = new LinkedEntry<>() ;
    }


    /**
     *LinkedEntry adds nxt/prv double-links to plain HashMapEntry
     * MyHashMap:
     * -HashMapEntry []
     * HashMapEntry:
     * -key
     * -value
     * -hash
     * -HashMapEntry next
     *
     * LinkedEntry:(extends HashMapEntry)
     * -LinkedEntry nxt
     * -LinkedEntry prv
     */
    static class LinkedEntry<K,V> extends HashMapEntry<K,V>{
        LinkedEntry<K,V> nxt ;
        LinkedEntry<K,V> prv ;

        //Create the header entry
        LinkedEntry(){
            super(null, null, 0, null);
            nxt = prv = this ;
        }

        //Create a normal entry
        LinkedEntry(K key, V value, int hash, HashMapEntry<K,V> next ,
                    LinkedEntry<K,V> nxt, LinkedEntry<K,V> prv){
            super(key, value, hash, next);
            this.nxt = nxt ;
            this.prv = prv ;
        }
    }

    /**
     *returns the eldest entry in the map or {@code null} if the
     * map is empty
     */
    public Entry<K,V> eldest(){
        LinkedEntry<K,V> eldest = header.nxt;
        return  eldest!= header? eldest: null;
    }

    /**
     *Evicts eldest entry if instructed, creates a new entry and linksit in
     * as head of linked list. This method should call constructorNewEntry
     * (instead of duplicating code) if the performance of your VM permits.
     *
     * <p></p>
     */
    @Override
    void addNewEntry(K key, V value, int hash, int index){
        LinkedEntry<K,V> header = this.header;

        //remove eldest entry if instructed to do so
        LinkedEntry<K,V> eldest = header.nxt;
        if (eldest!= header && removeEldestEntry(eldest)){
            remove(eldest.key) ;
        }
        //create new entry, link it on to list, and put in into table
        LinkedEntry<K,V> oldTail = header.prv ;
        LinkedEntry<K,V> newTail = new LinkedEntry<>(
                key,value, hash, table[index],header,oldTail);
        table[index] = oldTail.nxt = header.prv = newTail ;

    }

    @Override
    void addNewEntryForNullKey (V value){
        LinkedEntry<K,V> header = this.header ;
        //remove the eldest entry if instructed to do so
        LinkedEntry<K,V> eldest = header.nxt ;
        if (eldest!= header && removeEldestEntry (eldest)){
            remove(eldest.key) ;
        }
        //create new entry ,link it on to list ,and put it into table
        LinkedEntry<K,V> oldTail = header.prv ;
        LinkedEntry<K,V> newTail = new LinkedEntry<K,V>(
                null, value,0, null, header, oldTail
        );
        entryForNullKey = oldTail.nxt = header.prv= newTail;
    }
    //without eviction
    @Override
    HashMapEntry<K,V> constructorNewEntry(
            K key ,V value, int hash, HashMapEntry<K,V> next){
        LinkedEntry<K,V> header = this.header ;
        LinkedEntry<K,V> oldTail =header.prv ;
        LinkedEntry<K,V> newTail = new
                LinkedEntry<K,V>(key, value,hash, next,header, oldTail) ;
        return  oldTail.nxt = header.prv = newTail ;
    }

    @Override
    public V get (Object key ){
        /*
        This method is overridden to eliminate the need for a polymorphic
        invocation in superclass at the expense of code duplication .
         */
        if (key == null){
            HashMapEntry<K,V> e = entryForNullKey ;
            if (e == null){
                return null ;
            }
            if (accessOrder ){
                makeTail((LinkedEntry<K,V>) e ) ;
            }
            return  e.value;
        }
        int hash = 0;//Collections.secondaryHash(key ) ;
        HashMapEntry<K,V> [] tab = table ;
        for (HashMapEntry<K,V> e= tab[hash&(tab.length-1)];
                e!= null; e = e.next){
            K eKey = e.key ;
            if (eKey == key||(e.hash == hash && key.equals(eKey))){
                if (accessOrder)
                    makeTail((LinkedEntry<K,V>)e);
                return e.value ;
            }
        }
        return  null ;
    }

    /**
     * Relinks the given entry to the tail of the list, under access ordering,
     * this method is invoked whenever the value of a pre-existing entry is read
     * by Map.get or modified by Map.put
     */
    private void makeTail(LinkedEntry<K,V> e ){
        //unlink e
        e.prv.nxt = e.nxt;
        e.nxt.prv = e.prv;
        //relink e as tail
        LinkedEntry<K,V> header = this.header ;
        LinkedEntry<K,V> oldTail = header.prv ;
        e.nxt = header ;
        e.prv = oldTail ;
        oldTail.nxt = header.prv = e ;
        modCount++;
    }

    @Override
    void preModify (HashMapEntry<K,V> e ){
        if (accessOrder){
            makeTail((LinkedEntry<K,V>) e);
        }
    }

    @Override
    void postRemove (HashMapEntry<K,V> e ){
        LinkedEntry<K,V> le = (LinkedEntry<K,V>) e ;
        le.prv.nxt = le.nxt;
        le.nxt.prv = le.prv ;
        le.nxt = le.prv = null ;//help GC for performance
    }

    @Override
    public boolean containsValue(Object value ){
        if (value == null){
            for (LinkedEntry<K,V> header = this.header, e=header.nxt;
                    e!= header ; e = e.nxt){
                if (e.value == null){
                    return  true ;
                }
            }
            return  false;
        }

        //value is non-null
        for (LinkedEntry<K,V> header = this.header, e = header.nxt;
                e!= header ;e = e.nxt){
            if (value.equals(e.value)){
                return  true ;
            }
        }
        return  false;

    }

    public void clear (){
        super.clear();

        //clear all links to help GC
        LinkedEntry<K,V> header = this.header;
        for (LinkedEntry<K,V> e = header.nxt;e!= header ;){
            LinkedEntry<K,V> nxt = e.nxt;
            e.nxt = e.prv= null ;
            e = nxt;
        }
        header.nxt= header.prv = header;
    }

    private abstract class LinkedHashIterator<T> implements Iterator<T>{
        LinkedEntry<K,V> next = header.nxt ;
        LinkedEntry<K,V> lastReturned = null ;
        int expectedModCount = modCount ;

        @Override
        public boolean hasNext() {
            return next!= header;
        }


        public final void remove() {
            if (modCount!= expectedModCount){
                throw  new ConcurrentModificationException() ;
            }
            if (lastReturned == null){
                throw new  IllegalStateException();
            }
            MyLinkedHashMap.this.remove(lastReturned.key) ;
            lastReturned = null ;
            expectedModCount = modCount ;
        }
        final LinkedEntry<K,V> nextEntry (){
            if (modCount!= expectedModCount)
                throw  new ConcurrentModificationException() ;
            LinkedEntry<K,V> e = next ;
            if (e == header)
                throw  new NoSuchElementException() ;
            next = e.nxt ;
            return  lastReturned = e ;
        }
    }

    private final class KeyIterator extends LinkedHashIterator<K>{
        public final K next (){
            return nextEntry().key ;
        }
    }

    private final class ValueIterator extends LinkedHashIterator<V>{
        public final V next (){
            return nextEntry().value;
        }
    }

    private final class EntryIterator extends LinkedHashIterator<MyMap.Entry<K,V>>{
        public final MyMap.Entry<K,V> next(){
            return  nextEntry() ;
        }
    }

    //override view iterator methods to generate correct iteration
    @Override
    Iterator<K> newKeyIterator(){
        return  new KeyIterator() ;
    }

    @Override
    Iterator<V> newValueIterator (){
        return  new ValueIterator() ;
    }

    @Override
    Iterator<MyMap.Entry<K,V>> newEntryIterator(){
        return new EntryIterator() ;
    }

    protected boolean removeEldestEntry(MyMap.Entry<K,V> eldest ){
        return false ;
    }

}








