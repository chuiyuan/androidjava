package chuiyuan.lsj.androidjava;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import chuiyuan.lsj.androidjava.adapter.MainAdapter;
import chuiyuan.lsj.androidjava.base.BaseActivity;
import chuiyuan.lsj.androidjava.view.AIDLActivity;
import chuiyuan.lsj.androidjava.view.CQueueActivity;
import chuiyuan.lsj.androidjava.view.CustumViewActivity;
import chuiyuan.lsj.androidjava.view.DexClassLoaderActivity;
import chuiyuan.lsj.androidjava.view.DownloadActivity;
import chuiyuan.lsj.androidjava.view.FruitAidlActivity;
import chuiyuan.lsj.androidjava.view.HelloNdkActivity;
import chuiyuan.lsj.androidjava.view.LRUActivity;
import chuiyuan.lsj.androidjava.view.ServiceSendbroadcastActivity;
import chuiyuan.lsj.androidjava.view.StringReplaceActivity;

public class MainActivity extends BaseActivity{
    private ListView listView;
    private List mList = new ArrayList();
    private Class [] mClasses = {
            StringReplaceActivity.class } ;

    @Override
    protected void findView() {
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void initView() {
        listView= (ListView)findViewById(R.id.listView) ;
        MainAdapter mainAdapter = new MainAdapter(this, getData()) ;
        listView.setAdapter(mainAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, position+":"+id) ;
                switch (position){
                    case 0:
                        Intent intent = new Intent(getBaseContext(), StringReplaceActivity.class) ;
                        startActivity(intent);
                        break;
                    case 1:
                        Intent intent1 = new Intent(getBaseContext(), DexClassLoaderActivity.class) ;
                        startActivity(intent1);
                        break;
                    case 2:
                        Intent intent2 = new Intent(getBaseContext(), CQueueActivity.class) ;
                        startActivity(intent2);
                        break;
                    case 3:
                        Intent intent3= new Intent(getBaseContext(), DownloadActivity.class) ;
                        startActivity(intent3);
                        break;
                    case 4:
                        Intent intent4 = new Intent(getBaseContext(), ServiceSendbroadcastActivity.class) ;
                        startActivity(intent4);
                        break;
                    case 5:
                        Intent intent5 = new Intent(getBaseContext(), AIDLActivity.class) ;
                        startActivity(intent5);
                        break;
                    case 6:
                        Intent intent6 = new Intent(getBaseContext(), FruitAidlActivity.class) ;
                        startActivity(intent6);
                        break;
                    case 7:
                        Intent intent7 = new Intent(getBaseContext(), HelloNdkActivity.class) ;
                        startActivity(intent7);
                        break;
                    case 8:
                        Intent intent8 = new Intent(getBaseContext(), LRUActivity.class) ;
                        startActivity(intent8);
                        break;
                    case 9:
                        Intent intent9 = new Intent(getBaseContext(), CustumViewActivity.class) ;
                        startActivity(intent9);
                        break;
                    default:
                        break;
                }
            }
        });
    }

    @Override
    protected void setOnClickListener() {

    }

    @Override
    public void onClick(View v) {

    }

    public List getData (){
        mList.add("String Replace") ;
        mList.add("DexClassLoader");
        mList.add("CQueue");
        mList.add("DownloadService") ;
        mList.add("BroadcastService");
        mList.add("AIDLService");
        mList.add("FruitService");
        mList.add("HelloNDK");
        mList.add("LRUActivity") ;
        mList.add("CumstumView");
        return  mList ;
    }
}
