package chuiyuan.lsj.androidjava.service;

import android.os.Binder;
import android.os.Parcel;
import android.os.RemoteException;

/**
 * Created by lsj on 2015/9/17.
 */
public class MusicPlayerService extends Binder {
    @Override
    protected boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
        //code表示想调用服务端的那个方法
        switch (code) {
            case 1000:
                //一种校验
                data.enforceInterface("MusicPlayerService");
                String filePath = data.readString();
                start(filePath);
                break;
        }
        return super.onTransact(code, data, reply, flags);
    }

    public void start(String filePath){

    }
    public void stop (){

    }
}
