package chuiyuan.lsj.androidjava.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;

import chuiyuan.lsj.androidjava.Fruit;
import chuiyuan.lsj.androidjava.IFruitBinder;

/**
 * Created by lsj on 2015/9/16.
 */
public class FruitAidlService extends Service {
    private Fruit mFruit ;

    @Override
    public void onCreate() {
        super.onCreate();
        mFruit = new Fruit();
        mFruit.setName("apple");
        mFruit.setColor("red");
        mFruit.setNumber(10);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return serviceBinder;
    }

    /**
     * 分析IFruitBinder.java文件中的：
     * (1)Stub ：基于Binder类，实现了AIDL接口，主要同服务端来使用，定义为一个abstract类，因为具体的服务
     * 函数由程序员实现，所以aidl文件中定义的接口在stub类中并没有实现，同时Stub类中重载了onTransact
     * 方法，由于transact()方法内部给包裹写入参数的顺序是由aidl工具定义的，所以在onTransact()方法中，
     * AIDL工具自然知道按什么顺序从包裹中取出数据。
     * (2)Proxy类：客户端访问服务端的代理，所谓的代理主要 就是为了前面所提到的第二个重要的问题：统一包裹
     * 内写入数据的顺序。
     *
     * 这是是Server端，要实现真正的功能。
     */
    private IFruitBinder.Stub serviceBinder = new IFruitBinder.Stub() {
        @Override
        public String getInfo() throws RemoteException {
            return "I am a server";
        }

        @Override
        public Fruit getFruit() throws RemoteException {
            return mFruit;
        }
    } ;
}
