package chuiyuan.lsj.androidjava.base;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

/**
 * Created by lsj on 2015/8/30.
 */
public abstract class BaseContentAdapter<T> extends BaseAdapter{

    protected Context mContext ;
    protected List<T> mList ;
    protected LayoutInflater mInflater ;

    public BaseContentAdapter(Context mContext, List<T> mList){
        this.mContext = mContext ;
        this.mList = mList;
        mInflater = LayoutInflater.from(mContext) ;
    }

    @Override
    public int getViewTypeCount() {
        return super.getViewTypeCount();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public T getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getConvertView(position, convertView, parent);
    }

    public abstract View getConvertView(int position, View convertView, ViewGroup parent) ;
}
