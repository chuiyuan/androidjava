package chuiyuan.lsj.androidjava.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class DownloadService extends Service {
    private String TAG ="MainService" ;

    public class MyBinder extends Binder{
        public DownloadService getService(){
            return DownloadService.this;
        }
    }

    public void startDownload() throws InterruptedException{
        //可以看出，这里是在主线程，所以如果真的下载，应该另开一个线程
        Toast.makeText(DownloadService.this,"start download:"+Thread.currentThread().getName(),
                Toast.LENGTH_LONG).show();
        Thread.sleep(2);
        Toast.makeText(DownloadService.this, "download end", Toast.LENGTH_LONG).show();
    }

    //public MyBinder myBinder ;
    public DownloadService() {
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate");
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        super.onDestroy();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d(TAG, "onUnbind");
        return super.onUnbind(intent);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return  new MyBinder();
    }
}
