//
// Created by lsj on 2015/9/18.
//
#include <jni.h>
#include <android/log.h>

#ifndef LOG_TAG
#define LOG_TAG "ANDROID_LAB"
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)
#endif

//copy from header file
#ifndef _Included_chuiyuan_lsj_androidjava_jni_HelloNDK
#define _Included_chuiyuan_lsj_androidjava_jni_HelloNDK
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     chuiyuan_lsj_androidjava_jni_HelloNDK
 * Method:    syaHello
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_chuiyuan_lsj_androidjava_jni_HelloNDK_syaHello
        (JNIEnv *env, jobject jObj) {
    LOGE("log string from ndk");
    return (*env)->NewStringUTF(env, "Hello from JNI");
}

#ifdef __cplusplus
}
#endif
#endif

