package chuiyuan.lsj.androidjava.view;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.view.View;

import chuiyuan.lsj.androidjava.R;
import chuiyuan.lsj.androidjava.base.BaseActivity;
import chuiyuan.lsj.androidjava.service.DownloadService;

public class DownloadActivity extends BaseActivity{

    private DownloadService downloadService;
    private ServiceConnection sc = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            downloadService = ((DownloadService.MyBinder)service).getService();
            try{
                downloadService.startDownload();
            }catch (InterruptedException e){
                e.printStackTrace();
            }

        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    } ;
    @Override
    protected void findView() {
        setContentView(R.layout.activity_download);
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void setOnClickListener() {

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent bindIntent = new Intent(this, DownloadService.class);
        this.bindService(bindIntent, sc, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindService(sc);
    }

}
