package chuiyuan.lsj.androidjava.view;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.Map;

import chuiyuan.lsj.androidjava.R;
import chuiyuan.lsj.androidjava.base.BaseActivity;
import chuiyuan.lsj.androidjava.datastructrue.MyLRUCache1;

/**
 * Created by lsj on 2015/9/24.
 */
public class LRUActivity extends BaseActivity {
    MyLRUCache1<String ,String > cache = new MyLRUCache1<String, String >(3) ;
    TextView tv ;

    @Override
    protected void findView() {
        setContentView(R.layout.activity_cache);
    }

    @Override
    protected void initView() {
        tv = (TextView)findViewById(R.id.cache_text) ;
        StringBuilder sb = new StringBuilder() ;
        cache.put("1", "one");  //1
        cache.put("2", "two");   //2 1
        cache.put("3", "three"); //3 2 1
        cache.put("4", "four");  //4 3 2
        if (cache.get("2")== null)
            throw new Error() ; //2 4 3
        for (Map.Entry<String,String > e : cache.getAll()){
            sb.append(e.getKey()+":"+e.getValue()+"\n");
        }
        cache.put("5", "five"); //5 2 4
        cache.put("4", "second four");//4 5 2
        //verify cache content
        sb.append("cache used:"+cache.usedEntries()+"\n") ;
        sb.append(cache.get("4")+"\n");
        sb.append(cache.get("5")+"\n");
        sb.append(cache.get("2")+"\n");
        for (Map.Entry<String,String > e : cache.getAll()){
            sb.append(e.getKey()+":"+e.getValue()+"\n");
        }
        tv.setText(sb.toString());
        Log.d(TAG,sb.toString()) ;
     }

    @Override
    protected void setOnClickListener() {

    }

    @Override
    public void onClick(View v) {

    }
}
