package chuiyuan.lsj.androidjava.algorithm;

/**
 * Created by lsj on 2015/8/30.
 */
public class QuickSort {

    public QuickSort() {
    }

    public void quickSort(int [] a){
        quickSort(a, 0,a.length-1);
    }

    static final int CUTOFF= 10;

    public void quickSort(int []a, int left ,int right){
        if (left+ CUTOFF<= right){
            int pivot = median3(a, left,right) ;
            int i = left ;
            int j = right -1 ;
            for (;;){
                while(a[++i]>pivot){}
                while(a[--j]< pivot){}
                if (i<j){
                    swap(a, i, j);
                }else {
                    break;
                }
            }
            swap(a, i, right-1);//restore pivot
            quickSort(a, left, i-1);
            quickSort(a, i+1, right);

        }else {
            //插入排序
        }
    }

    public int median3(int []a, int left,int right){
        int center = (left+ right)/2 ;
        if (a[center]<a[left]){
            swap(a ,center, left);
        }
        if (a[right]<a[left]){
            swap(a,left , right);
        }
        if(a[center]> a[right]){
            swap(a,center, right);
        }
        //此时最大的元素在a[right],right = a.length-1
        //最小的已经在a[left]
        //我们将pivot放在a[right-1]上
        swap(a, center, right-1);
        return  a[right-1] ;
    }
    public void swap(int []a, int i,int j){
        int temp = a[i];
        a[i] = a[j] ;
        a[j] = temp ;
    }



}
