package chuiyuan.lsj.androidjava.designmode;

/**
 * Created by lsj on 2015/9/5.
 */
public class Singleton {
    private static Singleton instance = null;

    private Singleton(){

    }

    public static Singleton getInstance(){
        if (instance == null){
            syncInit();
        }
        return  instance;
    }

    /**
     * 静态方法在访问本类的成员时，只允许访问静态成员（即静态成员变量和静态方法），
     * 而不允许访问实例成员变量和实例方法；实例方法则无此限制。
     */
    private static synchronized void syncInit (){
        if (instance == null){
            instance = new Singleton();
        }
    }

}
