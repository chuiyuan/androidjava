package chuiyuan.lsj.androidjava.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import chuiyuan.lsj.androidjava.IMyAIDL;
import chuiyuan.lsj.androidjava.R;
import chuiyuan.lsj.androidjava.view.AIDLActivity;

/**
 * Created by lsj on 2015/9/12.
 */
public class AIDLService extends Service{
    boolean flag ;
    private final static String TAG ="AIDLService";

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind") ;
        Bundle bundle = intent.getExtras();
        flag = bundle.getBoolean("flag");
        return ms ;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");//此时会显示一个notification
        Notification notification = new Notification(R.drawable.icon_ok,"msg from service",
                System.currentTimeMillis()) ;
        Intent intent = new Intent(this, AIDLActivity.class) ;
        PendingIntent pi = PendingIntent.getActivity(this,0,intent,0);
        notification.setLatestEventInfo(this,"AIDLDemo", "running", pi);
        startForeground(1,notification);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG,"onStartCommand") ;
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.d(TAG, "onDestroy and stop thread") ;
        flag = false; //以停止线程
    }

    /**
     * 分析IMyAIDL.java文件中的：
     * (1)Stub ：基于Binder类，实现了AIDL接口，主要同服务端来使用，定义为一个abstract类，因为具体的服务
     * 函数由程序员实现，所以aidl文件中定义的接口在stub类中并没有实现，同时Stub类中重载了onTransact
     * 方法，由于transact()方法内部给包裹写入参数的顺序是由aidl工具定义的，所以在onTransact()方法中，
     * AIDL工具自然知道按什么顺序从包裹中取出数据。
     * (2)Proxy类：客户端访问服务端的代理，所谓的代理主要 就是为了前面所提到的第二个重要的问题：统一包裹
     * 内写入数据的顺序。
     *
     * 这是Server端，要实现真正的功能。
     */
    IMyAIDL.Stub ms = new IMyAIDL.Stub(){
        @Override
        public void basicTypes(int anInt, long aLong, boolean aBoolean, float aFloat, double aDouble, String aString) throws RemoteException {

        }

        @Override
        public void downLoad() throws RemoteException {
            new Thread(new Runnable() {
                int i =0;
                @Override
                public void run() {
                    while(flag){
                        try {
                            i++ ;
                            Log.d(TAG, "i的值是:"+i);
                            Thread.sleep(1000);
                        }catch (InterruptedException e ){
                            e.printStackTrace();
                        }
                    }
                }
            }).start();
        }
    };
}
