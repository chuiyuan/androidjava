package chuiyuan.lsj.androidjava.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import chuiyuan.lsj.androidjava.R;
import chuiyuan.lsj.androidjava.jni.HelloNDK;

public class HelloNdkActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hello_ndk);
        HelloNDK helloNDK = new HelloNDK();
        String s = helloNDK.syaHello();
        Toast.makeText(this,s,Toast.LENGTH_LONG).show();
    }

}
