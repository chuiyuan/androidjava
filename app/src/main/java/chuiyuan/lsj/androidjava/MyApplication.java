package chuiyuan.lsj.androidjava;

import android.app.Application;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

/**
 * Created by lsj on 2015/9/7.
 */
public class MyApplication extends Application {
    private  final String TAG = MyApplication.this.getClass().getSimpleName() ;

    private RequestQueue mRequestQueue ;
    private ImageLoader mImageLoader ;

    private static MyApplication mInstance ;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = null ;
    }
    public static synchronized MyApplication getInstance(){
        return  mInstance ;
    }

    public RequestQueue getRequestQueue(){
        if (mRequestQueue == null){
            mRequestQueue = Volley.newRequestQueue(MyApplication.this) ;
        }
        return  mRequestQueue ;
    }

    public ImageLoader getImageLoader(){
        getRequestQueue() ;
        if (mImageLoader== null){
            mImageLoader = new ImageLoader(this.mRequestQueue, null) ;// not ok
        }
        return  mImageLoader ;
    }

    public <T> void addToRequestQueue(Request<T> req ,String tag ){
        //set the default tag if tag is empty
        req.setTag(null) ; // not ok
        getRequestQueue().add(req) ;
    }

    public <T> void addToRequstQueue(Request<T> req ){
        req.setTag(TAG) ;
        getRequestQueue().add(req) ;
    }

    public void cancelPendingRequests (Object tag ){
        if (mRequestQueue!= null){
            mRequestQueue.cancelAll(tag);
        }
    }
}
























