package chuiyuan.lsj.androidjava.datastructrue;


import java.util.Hashtable;

/**
 * Created by lsj on 2015/9/24.
 * 使用双链表+HashTable
 * 原理：
 * 将Cache所有的位置用双链表连接起来。
 * 1.当一个位置命中后，就通过调整链表的指向，将这个位置的调整到链表头的位置。
 * 2.新加入的直接加到链表头中。
 * 这样在多次进行Cache操作后，最近被命中的，就会向链表头方向移动，而没有命中的，向链表后移动。
 * 要更换内容时，链表最后位置就是最少被命中的位置，我们只要淘汰链表最后的部分就可。
 */
public class MyLRUCache2<K,V> {
    private class Entry {
        Entry prev ;
        Entry next ;
        V value ;
        K key ;
    }

    private int cacheSize ;
    private Hashtable<K, Entry> nodes ; //cache container
    private int currentSize ;
    private Entry first ; //header
    private Entry last ; //last

    public MyLRUCache2(int cacheSize ){
        currentSize =0;
        this.cacheSize = cacheSize ;
        nodes = new Hashtable<>(cacheSize);
    }

    /**
     *得到cache中的对象，并放到最前面
     */
    public Entry get(K key ){
        Entry node = nodes.get(key) ;
        if (node!= null){
            moveToHead(node);
            return  node;
        }else {
            return  null ;
        }
    }

    /**
     *添加entry到hashtable
     */
    public void put(K key, V value){
        //先查看hashtable是否存在这个entry,如果存在，则只更新其value
        Entry node = nodes.get(key) ;

        if (node == null){
            //缓存容器是否已经超过大小
            if (currentSize>= cacheSize){
                nodes.remove(last.key) ;
                removeLast();
            }else {
                currentSize++;
            }
            node= new Entry();
        }
        node.value = value;
        //将最新使用的节点放到链表头最新使用的
        moveToHead(node) ;
        nodes.put(key,node);
    }
    /**
     * 将entry删除，删除只在cache满了才会被执行
     */
    public void remove(K key){
        Entry node = nodes.get(key) ;
        //在链表中删除
        if (node!= null){
            if (node.prev!= null){
                node.prev.next = node.next ;
            }
            if (node.next!= null){
                node.next.prev = node.prev ;
            }
            if (last== node){
                last = node.prev ;
            }
            if (first == node){
                first = node.next ;
            }
        }
        //在hashtable中删除
        nodes.remove(key) ;
    }
    /**
     * 删除链表尾部节点，也就是最少使用的缓存对象
     */
    private void removeLast(){
        // 链表尾部不为空，则将其指向空，删除链表尾部，
        if (last!= null){
            if (last.prev!= null){
                last.prev.next = null ;
            }else {
                first = null ;
            }
            last= last.prev ;
        }
    }
    /**
     * 移动到链表头，表示这个节点是最新使用过的
     */
    private void moveToHead( Entry node){
        if (node == first)
            return;
        if (node.prev!= null){
            node.prev.next = node.next;
        }
        if (node.next!= null){
            node.next.prev = node.prev;
        }
        if (last== node){
            last = node.prev ;
        }
        if (first!= null) {
            node.next = first ;
            first.prev = node;
        }
        first = node;
        node.prev = null ;
        if (last == null){
            last = first ;
        }
    }

    public void clear (){
        first = null ;
        last = null ;
        currentSize =0;
    }

}















