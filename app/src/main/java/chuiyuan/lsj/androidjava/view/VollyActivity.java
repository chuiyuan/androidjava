package chuiyuan.lsj.androidjava.view;

import android.app.ProgressDialog;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import chuiyuan.lsj.androidjava.MyApplication;
import chuiyuan.lsj.androidjava.base.BaseActivity;

/**
 * Created by lsj on 2015/9/7.
 * 在这之前，我们在程序中需要和网络通信的时候，大体使用的东西莫过于
 * AsyncTaskLoader，HttpURLConnection，AsyncTask，HTTPClient（Apache）等
 * 功能 ：
 * 1.json,图片(异步）
 * 2.网络请求的排序
 * 3.网络请求的优先级处理
 * 4.缓存
 * 5.各级别的取消请求
 * 6.与Activity生命周期联动
 */
public class VollyActivity  extends BaseActivity{

    NetworkImageView networkImageView ;
    ImageView imageView ;
    //PopupWindow m;
    @Override
    protected void findView() {


    }

    @Override
    protected void initView() {

    }

    @Override
    protected void setOnClickListener() {

    }

    @Override
    public void onClick(View v) {

    }

    /**
     * 创建json object请求
     */
    public void requestJson(){
        String tagJsonReq = "json_obj_req" ;
        String url = "http://api.androidhive.info/volley/person_object.json";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG , response.toString()) ;
                    }
                }, new Response.ErrorListener() {
                     @Override
             public void onErrorResponse(VolleyError error) {
                         Log.d(TAG , "error"+error.getMessage()) ;
            }
        }) ;

        MyApplication.getInstance().addToRequestQueue(jsonObjectRequest, tagJsonReq);
    }

    /**
     * 创建String请求
     */
    public void requestString (){
        String tagStringReq = "string_req" ;

        String url =  "http://api.androidhive.info/volley/string_response.html";

        final ProgressDialog progressDialog = new ProgressDialog(this) ;
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, response.toString()) ;
                        progressDialog.hide();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "error:"+ error.getMessage());
                progressDialog.hide();
            }
        }) ;
        MyApplication.getInstance().addToRequestQueue(stringRequest,tagStringReq);
    }

    /**
     * 上面都是GET
     */
    public void postJson (){
        String tagJsonObj = "json_obj_req" ;
        String  url = "http://api.androidhive.info/volley/person_object.json";

        final ProgressDialog progressDialog= new ProgressDialog(this) ;
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        progressDialog.hide();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                progressDialog.hide();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
               Map<String, String> params = new HashMap<>() ;
                params.put("name", "Androidhive") ;
                params.put("email", "abc@androidhive.info");
                params.put("password", "password123");
                return  params ;
            }
        };
        MyApplication.getInstance().addToRequestQueue(jsonObjectRequest, tagJsonObj);
    }

    /**
     * 添加请求头部信息
     */
    public void addHeader(){
        String tagJsonObj = "json_obj_req" ;
        String url = "http://api.androidhive.info/volley/person_object.json";;

        ProgressDialog progressDialog = new ProgressDialog(this) ;
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("apiKey", "xxxxxxxxxxxxxxx");
                return headers;
            }
        } ;
        MyApplication.getInstance().addToRequestQueue(jsonObjectRequest, tagJsonObj);
    }
    /**
     * 创建Image请求
     * 1.用NetWorkImageView自动加载
     * 2.用ImageView加载
     */
    public void loadByNetWorkImageView (){
        String url ="http://img4.imgtn.bdimg.com/it/u=795021948,3304593244&fm=21&gp=0.jpg";
        ImageLoader imageLoader = MyApplication.getInstance().getImageLoader();
        networkImageView.setImageUrl(url,imageLoader);
    }
    public void loadByImageView (){
        String url ="http://dl.pinyin.sogou.com/cache/skins/uploadImage/2013/03/24/13640988701276_former.jpg";
        ImageLoader imageLoader = MyApplication.getInstance().getImageLoader() ;
        // if using normal ImageView
        imageLoader.get(url, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                if (response.getBitmap()!= null){
                    imageView.setImageBitmap(response.getBitmap());
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Image Load Error: " + error.getMessage());
            }
        }) ;
        //可以简单些
        //imageLoader.get(url,
          //      ImageLoader.getImageListener(imageView,R.drawable.icon_ok , R.drawable.icon_error)) ;

    }

}


